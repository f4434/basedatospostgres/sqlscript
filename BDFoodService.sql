Buenas noches estimados anexo QUERY CON LAS tablas de la base de datos para el food services

Base de datos: Postgress

DICCIONARIO DE DATOS:

id -> GENERADOR DE SECUENCIA EJ 0, 1, 2, 3
name -> NOMBRE DEL PRODUCTOS O CATEGORIA
descripcion -> DETALLE DEL PLATILLO ES DECIR QUE CONTIENE
monto -> MONTO DEL PLATILLO
identificador -> RELACION CON LAS TABLAS ES DECIR PIZZAS (01) LOS PRODUCTOS O PLATILLOS DEBE TENER MARGARITA (01)
empresa -> NOMBRE DE EMPRESA O RESTAURANT RIF O NOMBRE ES PARA USO ADMINISTRATIVO USO INTERNO MAS QUE TODO
numero_orden -> NUMERO DE COMPROBANTE DONDE SE GENERA CUANDO EL MESERO ESCANEA EL QR DEL CLIENTE
comentario -> ALGUN COMENTARIO DEL PLATILLO ASI SEA BUENO O MALO, RECOMENDACIONES
plato -> NOMBRE DEL PRODUCTOS
name_client -> NOMBRE DEL CLIENTE
name_operator -> NOMBRE DEL MESERO QUE ESCANEO EL QR
numero_mesa -> NUMNERO DE MESA DONDE EL CLIENTE SOLICITO LA ORDEN
total_amount -> MONTO TOTAL A CANCELAR
ADMINISTRADOR (PARA MESERO)

- tabla con el objetivo de almacenar los platillos que el cliente solicito.

CREATE TABLE ordenes (
	id int8 NOT NULL,
	comentario varchar(255) NULL,
	monto int4 NULL,
	numero_orden varchar(255) NULL,
	plato varchar(255) NULL,
	CONSTRAINT ordenes_pkey PRIMARY KEY (id)
);

- tabla con el objetivo de guardar la orden del cliente con los montos a pagar y todo.

CREATE TABLE historico (
	id int8 NOT NULL,
	name_client varchar(255) NULL,
	name_operator varchar(255) NULL,
	numero_mesa varchar(255) NULL,
	numero_orden varchar(255) NULL,
	total_amount int4 NULL,
	CONSTRAINT historico_pkey PRIMARY KEY (id)
);

- tabla con el objetivo de guardas las categorias del cliente


CREATE SEQUENCE sq_category
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;
	
CREATE TABLE categoria (
	id int8 NOT NULL,
	empresa varchar(255) NULL,
	identificador varchar(255) NULL,
	"name" varchar(255) NULL,
	CONSTRAINT categoria_pkey PRIMARY KEY (id)
);


- tabla con el objetivo de almacenar la lista de todos los productos del comercio o restaurat es decir los platillos

CREATE SEQUENCE public.sq_products
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;
	
CREATE TABLE public.productos (
	id int8 NOT NULL,
	descripcion varchar(255) NULL,
	empresa varchar(255) NULL,
	identificador varchar(255) NULL,
	monto int4 NULL,
	"name" varchar(255) NULL,
	CONSTRAINT productos_pkey PRIMARY KEY (id)
);

